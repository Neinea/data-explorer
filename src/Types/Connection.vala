/* Connection.vala
 *
 * Copyright 2024 Thibault VALLOIS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace DataExplorer.Types {
    public class Connection : Object {
        public string id {get; construct;}
        public Types.DataSource connection_type {get; construct;}
        public string username {get; construct;}
        public string password {get; construct;}
        public string host {get; construct;}
        public string group_server_name {get; construct;}
        public string default_database {get; construct;}
        public int port {get; construct;}


        public Connection (DataExplorer.Services.ConnectionBuilder builder) {
            Object(
                id: builder.id,
                connection_type: builder.connection_type,
                username: builder.username,
                password: builder.password,
                host: builder.host,
                group_server_name: builder.group_server_name,
                default_database: builder.default_database,
                port: builder.port
            );
        }
    }
}
