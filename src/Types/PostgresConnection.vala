/* PostgresConnection.vala
 *
 * Copyright 2024 Thibault VALLOIS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
namespace DataExplorer.Types {
    class PostgresConnection : Object, Connectable {
        private Connection connection;

        public PostgresConnection (Connection connection) {
            this.connection = connection;
        }

        public async DataExplorer.Types.ConnectionResult start_connection () {
            return new DataExplorer.Types.ConnectionResult(true, "success");
        }

        public async DataExplorer.Types.ConnectionResult test_connection () {
            Postgres.ConnectionStatus? db_status = null;
            string? error_message = null;
            SourceFunc callback = test_connection.callback;
            var db = Postgres.connect_db (build_uri ());
            new Thread<bool> ("threaded-connection", () => {
                while (db_status == null && (db_status != Postgres.ConnectionStatus.OK || db_status != Postgres.ConnectionStatus.BAD)) {
                    db_status = db.get_status ();
                    error_message = db.get_error_message ();
                }
                GLib.Idle.add ((owned) callback);
                return true;
            });

            yield;
            return new DataExplorer.Types.ConnectionResult (db_status == Postgres.ConnectionStatus.OK, error_message);
        }

        private string build_uri () {
            return @"postgresql://$(connection.username):$(connection.password)@$(connection.host):$(connection.port)/postgres";
        }
    }
}
