/* MainWindow.vala
 *
 * Copyright 2024 Thibault VALLOIS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace DataExplorer {
    public class MainWindow : Adw.ApplicationWindow  {
        private Adw.NavigationSplitView main_view;
        private DataExplorer.Widgets.HeaderBar header_bar;
        private DataExplorer.Widgets.Workspace workspace;
        private DataExplorer.Widgets.SideBar sidebar;
        private GLib.Settings settings;

        public MainWindow (Adw.Application application) {
            Object (
                application: application
            );
        }

        construct {
            settings = new GLib.Settings ("com.thibaultvallois.DataExplorer");
            load_window_size ();
            var box = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);
            header_bar = new DataExplorer.Widgets.HeaderBar ();
            main_view = new Adw.NavigationSplitView () {
                vexpand = true
            };
            workspace = new DataExplorer.Widgets.Workspace ();
            sidebar = new DataExplorer.Widgets.SideBar ();
            header_bar.add_connection_button_clicked.connect (on_add_connection_button_clicked);
            var workspace_navigation_page = new Adw.NavigationPage (workspace, "workspace");
            var sidebar_navigation_page = new Adw.NavigationPage (sidebar, "sidebar");
            main_view.set_content (workspace_navigation_page);
            main_view.set_sidebar (sidebar_navigation_page);
            box.append (header_bar);
            box.append (main_view);

            set_content(box);
        }

        private void on_add_connection_button_clicked () {
            var main_window = get_application ().get_active_window ();
            var creation_process_window = new DataExplorer.Widgets.CreationProcess.MainWindow (main_window);
            creation_process_window.present ();
        }

        public override bool close_request () {
           save_window_size ();
           return false;
        }

        private void load_window_size () {
           var width = settings.get_int ("window-width");
           var height = settings.get_int ("window-height");
           var is_maximized = settings.get_boolean ("is-maximized");

           message ("loaded width %d, height %d", width, height);

           set_default_size (width, height);
           if (is_maximized) {
               maximize ();
           }
        }

        private void save_window_size () {
           int width;
           int height;
           get_default_size (out width, out height);
           message ("width %d, height %d", width, height);
           var is_maximized = is_maximized ();
           message ("is_maximized %b", is_maximized);
           settings.set_int ("window-width", width);
           settings.set_int ("window-height", height);
           settings.set_boolean ("is-maximized", is_maximized);
        }
    }
}
