/* Application.vala
 *
 * Copyright 2024 Thibault VALLOIS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace DataExplorer {
    public class Application : Adw.Application {
        public Application () {
            Object (application_id: "com.thibaultvallois.DataExplorer", flags: ApplicationFlags.DEFAULT_FLAGS);
        }

        construct {
            application_id = "com.thibaultvallois.DataExplorer";
            ActionEntry[] action_entries = {
                { "about", this.on_about_action },
                { "preferences", this.on_preferences_action },
                { "quit", this.quit }
            };
            this.add_action_entries (action_entries, this);
            this.set_accels_for_action ("app.quit", {"<primary>q"});
        }

        public override void activate () {
            base.activate ();
            try {
                init_connection_config_file ();
            } catch (GLib.FileError error) {
                var dlg = inform ("Error", "Unable to store connections credentials", this.active_window);
                dlg.present ();
            }
            var win = this.active_window;
            if (win == null) {
                win = new MainWindow (this);
            }
            win.present ();
        }

        private void init_connection_config_file () throws GLib.FileError {
            var file_name_path = DataExplorer.Services.ConnectionConfig.file_name_path;
            if (FileUtils.test (file_name_path, FileTest.EXISTS)) {
                return;
            }
            var key_file = new KeyFile ();
            key_file.save_to_file (file_name_path);
        }

		public Adw.MessageDialog inform (string text, string? msg = null, Gtk.Window? win = null) {
			var dlg = new Adw.MessageDialog (
				win,
				text,
				msg
			);

			if (win != null)
				dlg.transient_for = win;

			dlg.add_response ("ok", "OK");

			return dlg;
		}

        private void on_about_action () {
            string[] developers = { "Thibault VALLOIS" };
            var about = new Adw.AboutWindow () {
                transient_for = this.active_window,
                application_name = "data-explorer",
                application_icon = "com.thibaultvallois.DataExplorer",
                developer_name = "Thibault VALLOIS",
                version = "0.1.0",
                developers = developers,
                copyright = "© 2024 Thibault VALLOIS",
            };
            about.present ();
        }

        private void on_preferences_action () {
            message ("app.preferences action activated");
        }
    }
}

public static int main(string[] args) {
    return new DataExplorer.Application ().run (args);
}
