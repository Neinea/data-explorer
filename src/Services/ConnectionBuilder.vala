/* ConnectionBuilder.vala
 *
 * Copyright 2024 Thibault VALLOIS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace DataExplorer.Services {
    public class ConnectionBuilder : Object {
        public string id;
        public Types.DataSource connection_type;
        public string username;
        public string password;
        public string host;
        public string group_server_name;
        public string default_database;
        public int port;

        public ConnectionBuilder () {
            this.id = Uuid.string_random ();
        }

        public ConnectionBuilder add_username (string username) {
            this.username = username;
            return this;
        }

        public ConnectionBuilder add_password (string password) {
            this.password = password;
            return this;
        }

        public ConnectionBuilder add_host (string host) {
            this.host = host;
            return this;
        }

        public ConnectionBuilder add_port (int port) {
            this.port = port;
            return this;
        }

        public ConnectionBuilder add_connection_type (Types.DataSource connection_type) {
            this.connection_type = connection_type;
            return this;
        }

        public ConnectionBuilder add_default_database (string default_database) {
            this.default_database = default_database;
            return this;
        }

        public ConnectionBuilder add_group_server_name (string group_server_name) {
            this.group_server_name = group_server_name;
            return this;
        }

        public DataExplorer.Types.Connection build () {
            return new DataExplorer.Types.Connection (this);
        }
    }
}
