/* ConnectionConfig.vala
 *
 * Copyright 2024 Thibault VALLOIS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace DataExplorer.Services {
    public class ConnectionConfig : GLib.Object {
        static ConnectionConfig? instance;

        KeyFile key_file;
        public static string file_name_path = Path.build_path (
                                                               Path.DIR_SEPARATOR_S,
                                                               GLib.Environment.get_user_config_dir (),
                                                               "connection-config.ini"
        );

        private ConnectionConfig () {
            key_file = new KeyFile ();
            try {
                key_file.load_from_file (file_name_path, GLib.KeyFileFlags.NONE);
            } catch (GLib.Error err) {
                GLib.error ("Unable to load connection config file %s", err.message);
            }
        }

        public static ConnectionConfig get_instance () {
            if (instance == null) {
                instance = new ConnectionConfig ();
            }
            return instance;
        }

        public bool store_connection (DataExplorer.Types.Connection connection) {
            key_file.set_
        }
    }
}
