/* ConnectionManager.vala
 *
 * Copyright 2024 Thibault VALLOIS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace DataExplorer.Services {
    public class ConnectionManager : Object {
        static ConnectionManager? instance;
        private Secret.Schema schema;

        private ConnectionManager () {
            this.schema = new Secret.Schema ("com.thibaultvallois.DataExplorer", Secret.SchemaFlags.NONE);
        }

        public static ConnectionManager get_instance () {
            if (instance == null) {
                instance = new ConnectionManager ();
            }
            return instance;
        }

        public void add (DataExplorer.Types.Connection connection) {

            //  Secret.password_store_sync (Secret.Schema schema, string? collection, string label, string password, GLib.Cancellable? cancellable, ...)
        }

        public async DataExplorer.Types.ConnectionResult test_connection (DataExplorer.Types.Connection connection) {
            var factory = new DataExplorer.Services.ConnectionFactory (connection);
            var conn = factory.build ();
            return yield conn.test_connection ();
        }
    }
}

