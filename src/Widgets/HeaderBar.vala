/* HeaderBar.vala
 *
 * Copyright 2024 Thibault VALLOIS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace DataExplorer.Widgets {
    public class HeaderBar : Adw.Bin {
        public signal void add_connection_button_clicked ();
        private Adw.HeaderBar internal_header_bar;
        private Gtk.Button add_connection_button;

        construct {
            init_ui ();
            add_connection_button.clicked.connect ((button) => {
                add_connection_button_clicked ();
            });
            set_child (internal_header_bar);
        }

        private void init_ui () {
            internal_header_bar = new Adw.HeaderBar ();
            add_connection_button = new Gtk.Button.from_icon_name ("list-add-symbolic");
            internal_header_bar.pack_start (add_connection_button);
        }
    }
}
