/* DataSourceChoices.vala
 *
 * Copyright 2024 Thibault VALLOIS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace DataExplorer.Widgets.CreationProcess {
    class DataSourceChoices : Adw.NavigationPage {
        private Gtk.Button next_button;
        private Adw.HeaderBar header_bar;
        private Adw.ToolbarView toolbar_view;
        private Gtk.CheckButton postgresql_check_button;
        private Gtk.CheckButton mysql_check_button;
        private Types.DataSource? selected_value = null;
        public signal void button_clicked (Types.DataSource selected_data_source);

        construct {
            set_title ("Select your data sources");
            build_ui ();
            initialize_signals ();
        }

        private void build_ui () {
            toolbar_view = new Adw.ToolbarView ();
            header_bar = new Adw.HeaderBar ();
            toolbar_view.add_top_bar (header_bar);

            var box = new Gtk.Box (Gtk.Orientation.VERTICAL, 10);
            box.margin_start = box.margin_end = 10;
            next_button = new Gtk.Button.with_label ("Next");
            next_button.halign = Gtk.Align.CENTER;
            next_button.add_css_class ("suggested-action");
            next_button.set_sensitive (false);
            var list_box = new Gtk.ListBox ();
            list_box.add_css_class ("boxed-list");
            var postgresql_choice = new Adw.ActionRow () {
                title = "PostgreSQL"
            };
            postgresql_check_button = new Gtk.CheckButton ();
            var mysql_choice = new Adw.ActionRow () {
                title = "MySQL"
            };
            mysql_check_button = new Gtk.CheckButton ();
            postgresql_choice.set_activatable_widget (postgresql_check_button);
            postgresql_choice.add_prefix (postgresql_check_button);
            postgresql_check_button.set_group (mysql_check_button);
            mysql_choice.set_activatable_widget (mysql_check_button);
            mysql_choice.add_prefix (mysql_check_button);
            list_box.append (postgresql_choice);
            list_box.append (mysql_choice);
            box.append (list_box);
            box.append (next_button);
            toolbar_view.set_content (box);
            set_child (toolbar_view);
        }

        private void initialize_signals () {
            postgresql_check_button.toggled.connect ((button) => {
                if (button.active) {
                    set_selected_value (Types.DataSource.POSTGRES);
                }
            });
            mysql_check_button.toggled.connect ((button) => {
                if (button.active) {
                    set_selected_value (Types.DataSource.MYSQL);
                }
            });
            next_button.clicked.connect (() => {
                button_clicked (selected_value);
            });
        }

        private void set_selected_value (Types.DataSource? data_source) {
            selected_value = data_source;
            next_button.set_sensitive (data_source != null);
        }
    }
}
