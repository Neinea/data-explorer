/* DataSourcePortEntryRow.vala
 *
 * Copyright 2024 Thibault VALLOIS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace DataExplorer.Widgets.CreationProcess {
    class DataSourcePortEntryRow : DataExplorer.Utils.ValidatedEntryRow {
        construct {
            initialize_signals ();
        }

        public override bool is_valid () {
            return text != "" && is_port_valid (text);
        }

        private bool is_port_valid (string data) {
            return int.try_parse(data) && int.parse(data) >= 1 && int.parse(data) <= 65535;
        }

        private void initialize_signals () {
            changed.connect ((data) => {
                remove_css_classes ();
                if (data.text == "") {
                    return;
                }

                if (is_port_valid (data.text)) {
                    if (!has_css_class ("success")) {
                        add_css_class ("success");
                    }
                    return;
                }

                add_css_class ("error");
            });
        }

        private void remove_css_classes () {
            remove_css_class ("success");
            remove_css_class ("error");
        }
    }
}
