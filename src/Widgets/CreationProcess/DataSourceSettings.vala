/* DataSourceSettings.vala
 *
 * Copyright 2024 Thibault VALLOIS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace DataExplorer.Widgets.CreationProcess {
    public class DataSourceSettings : Adw.NavigationPage {
        private Adw.HeaderBar header_bar;
        private Adw.ToolbarView toolbar_view;
        private EntryRowGroup entry_row_group;
        private DataExplorer.Widgets.CreationProcess.DataSourceEntryRow host_entry;
        private DataExplorer.Widgets.CreationProcess.DataSourcePortEntryRow port_entry;
        private DataExplorer.Widgets.CreationProcess.DataSourceEntryRow username_entry;
        private Adw.PasswordEntryRow password_entry;
        private DataExplorer.Widgets.CreationProcess.DataSourceEntryRow name_entry;
        private Gtk.Button create_button;
        private Gtk.Button test_connection_button;
        private Gtk.Label test_connection_result_label;
        private DataExplorer.Services.ConnectionBuilder connection_builder;
        private DataExplorer.Services.ConnectionManager connection_manager;
        public string host_value {get; set;}
        public string port_value {get; set;}
        public string username_value {get; set;}
        public string password_value {get; set;}
        public string name_value {get; set;}

        construct {
            set_title ("Select your data sources");
            connection_builder = new DataExplorer.Services.ConnectionBuilder ();
            connection_manager = DataExplorer.Services.ConnectionManager.get_instance ();
            build_ui ();
            initialize_bindings ();
            initialize_signals ();
        }

        private void build_ui () {
            var box = new Gtk.Box (Gtk.Orientation.VERTICAL, 10);
            box.margin_start = box.margin_end = 10;
            toolbar_view = new Adw.ToolbarView ();
            header_bar = new Adw.HeaderBar ();
            toolbar_view.add_top_bar (header_bar);

            var list_box = new Gtk.ListBox ();
            host_entry = new DataExplorer.Widgets.CreationProcess.DataSourceEntryRow () {
                title = "Host"
            };
            port_entry = new DataExplorer.Widgets.CreationProcess.DataSourcePortEntryRow () {
                title = "Port"
            };
            username_entry = new DataExplorer.Widgets.CreationProcess.DataSourceEntryRow () {
                title = "Username"
            };
            password_entry = new Adw.PasswordEntryRow () {
                title = "Password"
            };
            name_entry = new DataExplorer.Widgets.CreationProcess.DataSourceEntryRow () {
                title = "Database"
            };
            entry_row_group = new EntryRowGroup(
                new Gee.ArrayList<DataExplorer.Utils.ValidatedEntryRow>.wrap({
                    host_entry,
                    port_entry,
                    username_entry,
                    name_entry,
                })
            );
            list_box.append(host_entry);
            list_box.append(port_entry);
            list_box.append(username_entry);
            list_box.append(password_entry);
            list_box.append(name_entry);
            var buttons_box = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 10);
            buttons_box.halign = Gtk.Align.END;
            create_button = build_button ("Create");
            test_connection_button = build_button ("Test connection");
            var test_box = new Gtk.Box (Gtk.Orientation.VERTICAL, 10);
            test_connection_result_label = new Gtk.Label (null) {
                visible = false
            };
            test_connection_result_label.halign = Gtk.Align.FILL;
            test_box.append (test_connection_result_label);
            buttons_box.append (test_connection_button);
            buttons_box.append (create_button);
            box.append (list_box);
            box.append (buttons_box);
            box.append (test_box);
            toolbar_view.set_content (box);
            set_child (toolbar_view);
        }

        private Gtk.Button build_button (string label) {
            var button = new Gtk.Button.with_label (label);
            button.halign = Gtk.Align.CENTER;
            button.add_css_class ("suggested-action");
            button.set_sensitive (false);
            return button;
        }

        private void on_create_button_click () {
            fill_connection_builder ();
            connection_manager.add (connection_builder.build ());
        }

        private void on_test_connection_click () {
            fill_connection_builder ();
            connection_manager.test_connection.begin (connection_builder.build (), (obj, res) => {
                test_connection_result_label.visible = true;
                var test_connection_result = connection_manager.test_connection.end (res);
                if (test_connection_result.is_success) {
                    test_connection_result_label.set_text ("Connection success");
                    test_connection_result_label.add_css_class ("success");
                } else {
                    test_connection_result_label.set_text (@"Connection failed ($(test_connection_result.message))");
                    test_connection_result_label.add_css_class ("error");
                }
            });
        }

        private void fill_connection_builder () {
            connection_builder.add_host(host_value);
            connection_builder.add_port(int.parse(port_value));
            connection_builder.add_username(username_value);
            connection_builder.add_password(password_value);
            connection_builder.add_default_database (name_value);
        }

        private void initialize_bindings () {
            host_entry.bind_property ("text", this, "host_value", BindingFlags.DEFAULT);
            password_entry.bind_property ("text", this, "password_value", BindingFlags.DEFAULT);
            port_entry.bind_property ("text", this, "port_value", BindingFlags.DEFAULT);
            name_entry.bind_property ("text", this, "name_value", BindingFlags.DEFAULT);
            username_entry.bind_property ("text", this, "username_value", BindingFlags.DEFAULT);
        }

        private void initialize_signals () {
            entry_row_group.data_changed.connect (entry_row => {
                var is_form_fulfilled = entry_row_group.are_entry_rows_valid ();
                create_button.set_sensitive (is_form_fulfilled && password_entry.text != "");
                test_connection_button.set_sensitive (is_form_fulfilled && password_entry.text != "");
            });
            password_entry.changed.connect ((data) => {
                var has_text = data.text != "";
                if (has_text) {
                    if (!has_css_class ("success")) {
                        add_css_class ("success");
                    }
                } else {
                    remove_css_class ("success");
                    remove_css_class ("error");
                }
                var is_form_fulfilled = entry_row_group.are_entry_rows_valid ();
                create_button.set_sensitive (is_form_fulfilled && has_text);
                test_connection_button.set_sensitive (is_form_fulfilled && has_text);
            });

            test_connection_button.clicked.connect (on_test_connection_click);
            create_button.clicked.connect (on_create_button_click);
        }
    }
}
