/* MainWindow.vala
 *
 * Copyright 2024 Thibault VALLOIS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace DataExplorer.Widgets.CreationProcess {
    class MainWindow : Adw.Window {
        private Adw.NavigationView navigation_view;
        private DataSourceChoices data_source_choices_view;
        private DataSourceSettings data_source_settings_view;
        private DataExplorer.Services.ConnectionBuilder connection_builder;
        public unowned Gtk.Window? main_window { get; construct; }

        public MainWindow (Gtk.Window? main_window) {
            Object (
                modal: true,
                destroy_with_parent: true,
                default_width: 400,
                default_height: 400,
                main_window: main_window
            );
        }

        construct {
            connection_builder = new DataExplorer.Services.ConnectionBuilder ();
            if (main_window != null) {
                set_transient_for (main_window);
            }
            build_ui ();
            listen_signals ();
        }

        private void build_ui () {
            navigation_view = new Adw.NavigationView ();
            data_source_choices_view = new DataSourceChoices ();
            navigation_view.push (data_source_choices_view);
            set_content (navigation_view);
        }

        private void listen_signals () {
            data_source_choices_view.button_clicked.connect ((selected_value) => {
                connection_builder.add_connection_type (selected_value);
                data_source_settings_view = new DataSourceSettings ();
                navigation_view.push (data_source_settings_view);
            });
        }
    }
}
