/* EntryRowGroup.vala
 *
 * Copyright 2024 Thibault VALLOIS
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

class EntryRowGroup : Object {
    public Gee.ArrayList<DataExplorer.Utils.ValidatedEntryRow> entry_rows { private get; construct; }
    public signal void data_changed (DataExplorer.Utils.ValidatedEntryRow entry_row);

    public EntryRowGroup (Gee.ArrayList<DataExplorer.Utils.ValidatedEntryRow> entry_rows) {
        Object (entry_rows: entry_rows);
    }

    construct {
        initialize_signals ();
    }

    private void initialize_signals () {
        foreach (var entry_row in entry_rows) {
            entry_row.changed.connect ((_editable => {
                data_changed (entry_row);
            }));
        }
    }

    public bool are_entry_rows_valid () {
        return entry_rows.all_match (entry_row => entry_row.text != "" && entry_row.is_valid ());
    }
}
